var photo;

function preload() {
  photo = loadImage("stanley-1.jpg");
}

function setup() {
  createCanvas(400, 400);
  //photo = loadImage("stanley-1.jpg");

  //https://stackoverflow.com/questions/34433185/how-to-crop-an-image-in-p5
  maskImage = createGraphics(100, 100);
  maskImage.background(0, 0);
  //maskImage.fill(0, 255, 255, 0);
  maskImage.fill(0, 255);
  maskImage.noStroke();
  //maskImage.stroke(0, 200);
  //maskImage.ellipseMode(RADIUS);
  maskImage.ellipse(30, 30, 60, 60);
  maskImage.fill(0, 150);
  maskImage.rect(0, 30, 90, 20);

  // solo toma la capa alpha de la maskImage, los colores no los ve
  photo.mask(maskImage);
}

function draw() {
  background(255);
  background(255, 255, 0);
  image(photo, 0, 0, width, height);
  image(maskImage, 0, 200, width, height);
}
