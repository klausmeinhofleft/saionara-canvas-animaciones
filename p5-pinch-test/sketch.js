var diameter;
var distance;
var ex, ey;
var mc;

function setup() {
  var myCan = createCanvas(windowWidth, windowHeight);
  //var element = document.getElementById(myCan.elt);
  //console.log(myCan);
  mc = new Hammer(myCan.elt);
  mc.get("pinch").set({enable: true});

  diameter = 50;
  ex = int(windowWidth / 2);
  ey = int(windowHeight / 2);

  setupPinch();
}

function draw() {
  background(255);
  noStroke();
  fill(color(0));
  ellipse(ex, ey, diameter, diameter);
}

// https://github.com/hammerjs/hammer.js
var pinchStep = 1;
function setupPinch() {
  mc.on("pinchout pinchin", function(e) {
    //distance = int(dist(e.center.x, e.center.y, ex, ey));
    //console.log(e);
    //if (distance < diameter / 2) {
    if (e.type === "pinchin") {
      diameter = diameter - pinchStep;
    } else if (e.type === "pinchout") {
      diameter = diameter + pinchStep;
    }
    //}
  });
}
