class Mundo {
  constructor() {
    this.cx = 0;
    this.cy = 0;
    this.i = 0;
    this._radius = 0;
    this._imgOriginal = null;
    this._img = null;
  }

  draw() {
    image(this._img, this.cx - this._radius, this.cy - this._radius);
    this.cx = (this.cx + 3 * (1 + this.i)) % windowWidth;
  }

  setImg(img) {
    this._imgOriginal = this._img = img;
  }

  resizeRadius(val) {
    this._radius = val;

    this._img = createImage(this._imgOriginal.width, this._imgOriginal.height);

    //https://p5js.org/reference/#/p5.Image/copy
    // sx, sy, sw, sh, dx, dy, dw, dh
    this._img.copy(
      this._imgOriginal,
      0,
      0,
      this._imgOriginal.width,
      this._imgOriginal.height,
      0,
      0,
      this._imgOriginal.width,
      this._imgOriginal.height
    );

    this._img.resize(this._radius * 2, this._radius * 2);
  }
}
