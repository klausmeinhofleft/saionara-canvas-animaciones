var texSize = 120;
var texCutSrcSize = 50;
var texCutDstSize = 80;

// variables del degradé
var texDegradeN = 1;
var texSatMod = 30;
var texLightMod = 20;

function processImgs(imgs) {
  var texCenterXY = (texRadius = texSize / 2);
  for (var i = 0; i < imgs.length; i++) {
    var tex = imgs[i];

    var texCut = createImage(texCutDstSize, texCutDstSize);

    // copiamos una caja de X x Y píxeles de la textura original y la zoomeamos
    texCut.copy(
      tex,
      0,
      0,
      texCutSrcSize,
      texCutSrcSize,
      0,
      0,
      texCutDstSize,
      texCutDstSize
    );

    texCut.resize(texSize, texSize);

    for (y = 0; y < texCut.height; y++) {
      for (x = 0; x < texCut.width; x++) {
        var col = texCut.get(x, y);
        var newCol;

        // dibujamos el círculo
        var d = dist(texCenterXY, texCenterXY, x, y);
        if (d <= texRadius) {
          var h = hue(col),
            s = saturation(col),
            l = lightness(col);
          l += texLightMod;
          s += texSatMod;
          if (l > 100) l = 100;
          if (s > 100) s = 100;
          var rgb = HSLtoRGB(h / 360, s / 100, l / 100);
          var alpha =
            d < texRadius - texDegradeN ? 1.0 : (texRadius - d) / texDegradeN;
          newCol = color(rgb.r, rgb.g, rgb.b, alpha * 255);
        }
        // píxeles afuera del radio los hacemos transparentes (alpha=0)
        else newCol = color(col.v1, col.v2, col.v3, 0);

        texCut.set(x, y, newCol);

        // seteando los píxeles por array es más rápido
        /*var red = random(255);
          var green = random(255);
          var blue = random(255);
          writeColor(img, x, y, red, green, blue);*/
      }
    }
    texCut.updatePixels();

    imgs[i] = texCut;
  }
}

function resizeImgs(imgs, texSize) {
  for (var i = 0; i < imgs.length; i++) {
    var tex = imgs[i];
    tex.resize(texSize, texSize);
  }
}
