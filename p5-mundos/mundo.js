function copyImgRandomizedRGB(img) {
  // no modificamos la imagen original sino que hacemos copia
  var imgVar = createImage(img.width, img.height);
  //https://p5js.org/reference/#/p5.Image/copy
  imgVar.copy(img, 0, 0, img.width, img.height, 0, 0, img.width, img.height);
  imgVar.loadPixels();
  var maxColorOffset = 40;
  var rOffset = random() * maxColorOffset * 2 - maxColorOffset;
  var gOffset = random() * maxColorOffset * 2 - maxColorOffset;
  var bOffset = random() * maxColorOffset * 2 - maxColorOffset;
  for (x = 0; x < imgVar.width; x++) {
    for (y = 0; y < imgVar.height; y++) {
      var index = (x + y * imgVar.width) * 4;
      var r = constrain(imgVar.pixels[index] + rOffset, 0, 255);
      var g = constrain(imgVar.pixels[index + 1] + gOffset, 0, 255);
      var b = constrain(imgVar.pixels[index + 2] + bOffset, 0, 255);
      imgVar.pixels[index] = r;
      imgVar.pixels[index + 1] = g;
      imgVar.pixels[index + 2] = b;
      //imgVar.pixels[index + 3] = alpha;
      // otra forma más lenta
      //imgVar.set(x, y, color(r, g, b, a));
    }
  }
  imgVar.updatePixels();
  return imgVar;
}

class Mundo {
  constructor() {
    this.cx = 0;
    this.cy = 0;
    this.ax = 0;
    this.i = 0;
    this._radius = 0;
    this._rot = 0;
    this._imgOriginal = null;
    this._img = null;
  }

  draw() {
    push();
    imageMode(CENTER);
    //translate(this.cx - this._radius, this.cy - this._radius);
    translate(this.cx, this.cy);
    /*push();
    scale(0.5);
    image(this._img, -30, 0);
    pop();
    push();
    scale(0.8);
    image(this._img, -10, 0);
    pop();*/
    var radiusVar = this._radius / 30;
    strokeWeight(1);
    stroke(random() < 0.5 ? "green" : "red");
    noFill();
    //https://p5js.org/reference/#/p5/beginShape
    beginShape();
    vertex(0, 0);
    vertex(-20, random(-10, 10) * radiusVar);
    vertex(-40, random(-20, 20) * radiusVar);
    vertex(-60, random(-30, 30) * radiusVar);
    endShape();
    rotate(this._rot);
    //image(this._img, this.cx - this._radius, this.cy - this._radius);
    //https://p5js.org/reference/#/p5/imageMode
    image(this._img, 0, 0);
    //image(this._img, this._radius, this._radius);
    /*stroke("red");
    strokeWeight(3);
    line(0, 0, 50, 0);*/
    //this.cx = (this.cx + (1 + this.i)) % windowWidth;
    pop();

    this.cx += this.ax;

    // loop
    if (this.cx - this._radius > windowWidth) {
      this.cx = -this._radius;
    }

    this._rot = (this._rot + 0.05) % TWO_PI;
  }

  setImg(img) {
    this._imgOriginal = img;
    this._img = copyImgRandomizedRGB(img);
  }

  resizeRadius(val) {
    this._radius = val;

    /*this._img = createImage(this._imgOriginal.width, this._imgOriginal.height);

    //https://p5js.org/reference/#/p5.Image/copy
    // sx, sy, sw, sh, dx, dy, dw, dh
    this._img.copy(
      this._imgOriginal,
      0,
      0,
      this._imgOriginal.width,
      this._imgOriginal.height,
      0,
      0,
      this._imgOriginal.width,
      this._imgOriginal.height
    );*/

    this._img.resize(this._radius * 2, this._radius * 2);
  }
}
