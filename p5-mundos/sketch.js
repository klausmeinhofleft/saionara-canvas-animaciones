//var diameter;
var distance;
var ex, ey;
var mc;

var pinchStep = 1;
var pinching = false;
var fingerMoving = false;

var touchesColors = [];

var img,
  img2,
  loadingImage = true;

var imgs = [];
var imgsN = 4;
//var imgsN = 1;
var imgsLoading = true;

//var texSize = 18;
var texSizeMin = 8;
var texSizeMax = 28;

var defaultColorMode;

var mundos = [];
var mundosN = 16;

var zoom = 1;

var song;

var millistStart;

var poemLoopStart;
var poemTimes = [
  4000 /*vacío*/,
  2500 /*primer línea*/,
  2300,
  2000,
  200 /*fadeout*/,
  200,
  2000 /*vacío*/,
  1500,
  2000
];
var poemTimesSum = 0;
for (var i = 0; i < poemTimes.length; i++) poemTimesSum += poemTimes[i];
function timesAdd(i) {
  var tot = 0;
  for (; i >= 0; i--) {
    tot += poemTimes[i];
  }
  return tot;
}

function resetColorMode() {
  colorMode(defaultColorMode.modo, defaultColorMode.val);
}

function preload() {
  song = loadSound("rif.mp3");
}

function setup() {
  // RGB/HSB necesitan que cargue p5 antes
  defaultColorMode = {modo: RGB, val: 255};
  resetColorMode();

  var myCan = createCanvas(window.innerWidth, window.innerHeight);

  mc = new Hammer(myCan.elt);
  mc.get("pinch").set({enable: true});

  //diameter = 50;
  ex = int(windowWidth / 2);
  ey = int(windowHeight / 2);

  setupPinch();

  touchesColors = [
    color(244, 39, 0),
    color(24, 244, 0),
    color(24, 111, 244),
    color(205, 66, 122),
    color(244, 244, 0)
  ];
  //https://happycoding.io/tutorials/p5js/images
  //https://p5js.org/reference/#/p5.Image
  img = loadImage("stanley-1.jpg");
  //img = loadImage("https://happycoding.io/images/stanley-1.jpg");
  img2 = createImage(100, 100);

  for (var i = 0; i < imgsN; i++)
    imgs[i] = loadImage("tex-" + (i + 1) + ".jpg");

  for (var i = 0; i < mundosN; i++) mundos[i] = new Mundo();
}

function imgIsLoading(img) {
  return img.width == 1 || img.height == 1;
}

//https://p5js.org/reference/#/p5.Image
function writeColor(image, x, y, red, green, blue, alpha) {
  var index = (x + y * width) * 4;
  image.pixels[index] = red;
  image.pixels[index + 1] = green;
  image.pixels[index + 2] = blue;
  if (alpha !== undefined && alpha !== null) image.pixels[index + 3] = alpha;
}

function draw() {
  if (!millistStart) millistStart = millis();
  var millistElapsed = millis() - millistStart;

  resetColorMode();

  background(0);

  push(); //transforms mundos scene

  if (zoom != 1) {
    var zoomOffset = zoom - 1;
    translate(
      (-windowWidth * zoomOffset) / 2,
      (-windowHeight * zoomOffset) / 2
    );
  }
  scale(zoom);

  //## Lazy load de imágenes locas
  /*if (loadingImage && !imgIsLoading(img)) {
    //https://happycoding.io/tutorials/p5js/images
    //https://p5js.org/reference/#/p5.Image
    img.loadPixels();

    //https://p5js.org/reference/#/p5.Image/copy
    // sx, sy, sw, sh, dx, dy, dw, dh
    img2.copy(img, 0, 0, 100, 100, 0, 0, 100, 100);

    // tarda AÑOS en renderear en DDK browser...
    // Loop over every pixel in the image
    for (var y = 0; y < img.height; y++) {
      for (var x = 0; x < img.width; x++) {
        // Read the pixel's color
        var originalColor = img.get(x, y);

        // Inverse the color
        var r = 255 - red(originalColor);
        var g = 255 - green(originalColor);
        var b = 255 - blue(originalColor);
        var outputColor = color(r, g, b);

        // Set the pixel's color
        img.set(x, y, outputColor);
      }
    }
    img.updatePixels();
    /*var originalColor = img.get(0, 0);
    for (let y = 0; y < img.height; y++) {
      img.set(40, y, color(255, 0, 0));
      for (var i = 0; i < 10; i++) {
        img.set(50 + i, y, img.get(0, y));
      }
    }
    img.updatePixels();
    var originalColor = img.get(10, 0);
    for (var x = 0; x < img.width; x++) {
      img.set(x, 20, color(255, 0, 0));
      img.set(x, 30, img.get(x, 0));
    }
    img.updatePixels();*/

  //https://p5js.org/reference/#/p5.Image/resize
  /*img.resize(250, windowHeight);

    loadingImage = false;
  }*/

  //## Lazy load de imágenes de mundos
  if (imgsLoading) {
    // este bloque se ejecuta todos los frames iniciales
    var oneIsLoading = false;
    for (var i = 0; i < imgs.length; i++) {
      if (imgIsLoading(imgs[i])) {
        oneIsLoading = true;
        break;
      }
    }

    // si ninguna está cargando => terminaron todas
    // este bloque se ejecuta una sola vez
    if (!oneIsLoading) {
      imgsLoading = false;

      console.log("Texs cargadas", imgs);

      processImgs(imgs);

      console.log("Texs procesadas");

      for (var i = 0; i < mundos.length; i++) {
        var mundo = mundos[i];
        mundo.i = i;
        mundo.setImg(imgs[i % imgs.length]);

        var radius = random(texSizeMin, texSizeMax);
        mundo.resizeRadius(radius);
        //https://p5js.org/reference/#/p5/random
        mundo.cx = random(-radius * 2, windowWidth);
        //mundo.cy = 20 + i * 80;
        mundo.cy = random(radius, windowHeight - radius);
        mundo.ax = random(0.5, 8);
      }

      if (!song.isPlaying()) song.loop();
    } //endif !oneIsLoading
  }

  //## Dibujar cajita de zoom
  if (zoom < 1) {
    stroke("green");
    strokeWeight(2);
    noFill();
    rect(-1, -1, windowWidth + 2, windowHeight + 2);
  }

  //## Dibujar punto en el centro
  /*noStroke();
  //fill(color(0));
  colorMode(HSB, 100);
  fill(color(0, 50, 80));
  resetColorMode();
  ellipse(ex, ey, diameter, diameter);*/

  //## Dibujar imágenes locas
  //https://p5js.org/reference/#/p5/push
  /*push();
  rotate(-0.2);
  image(img, 0, 0);
  image(img2, -10, -10);
  pop();*/

  //## Dibujar mundos!
  if (!imgsLoading) {
    if (!poemLoopStart) poemLoopStart = millis();
    //fill("purple");
    fill(color(255, 0, 0));
    strokeWeight(3);
    stroke(color(100, 0, 100));
    //stroke("red");
    //noStroke();
    textSize(20);
    var txt1 = "detrás de cada mundo";
    var txt2 = "  yace un nuevo paradigma";
    var txt3 = "    por descubrir";
    var txt;
    var poemDiff = millistElapsed - poemLoopStart;
    var ti = 0;
    if (poemDiff < timesAdd(ti++)) txt = "";
    else if (poemDiff < timesAdd(ti++)) txt = txt1;
    else if (poemDiff < timesAdd(ti++)) txt = txt1 + "\n" + txt2;
    else if (poemDiff < timesAdd(ti++)) txt = txt1 + "\n" + txt2 + "\n" + txt3;
    else if (poemDiff < timesAdd(ti++)) txt = txt1 + "\n" + txt2;
    else if (poemDiff < timesAdd(ti++)) txt = txt1;
    else if (poemDiff < timesAdd(ti++)) txt = "";
    else if (poemDiff < timesAdd(ti++)) txt = "\n\n" + txt3;
    else if (poemDiff < timesAdd(ti++)) txt = "";
    else {
      //último frame antes de loopear
      poemLoopStart = millis();
      txt = "";
    }
    if (millis())
      text(txt, (2 * windowWidth) / 3, (3 * windowHeight) / 5, 300, 300);

    /*for (var i = 0; i < imgs.length; i++) {
      image(
        imgs[i],
        (windowWidth / 3 + (millis() / 50) * (i + 1)) % windowWidth,
        30 + i * (texSize + 5)
      );
    }*/

    for (var i = 0; i < mundos.length; i++) {
      var mundo = mundos[i];
      mundo.draw();
    }
  }

  //## Dibujar touch fingers/click
  if (!pinching && fingerMoving) {
    // https://p5js.org/reference/#/p5/touches
    // The touches[] array is not supported on Safari and IE on touch-based desktops (laptops).
    if (touches.length) {
      for (var i = 0; i < touches.length; i++) {
        fill(touchesColors[i % touchesColors.length]);
        ellipse(touches[i].x, touches[i].y, 120, 120);
      }
      strokeWeight(5);
      for (var i = 0; i < touches.length; i++) {
        stroke(touchesColors[i % touchesColors.length]);
        var thisXY = touches[i];
        var nextXY = i == touches.length - 1 ? touches[0] : touches[i + 1];
        //https://p5js.org/reference/#/p5/line
        line(thisXY.x, thisXY.y, nextXY.x, nextXY.y);
      }
    } else {
      fill(touchesColors[0]);
      ellipse(mouseX, mouseY, 120, 120);
    }
  } else fingerMoving = pinching = false;

  pop(); //transforms mundos scene

  //## Dibujar UI/msjs
  // mostrar después de mostrar poema entero
  if (millistElapsed > poemTimesSum + 1500) {
    fill(255);
    //stroke("green");
    noStroke();
    textSize(16);
    //https://p5js.org/reference/#/p5/text
    text(
      //"Para una mejor experiencia ocultá la barra del navegador o poné pantalla completa",
      //"Hacer zoom y tocar pantalla/click",
      "(zoom habilitado)",
      10,
      20,
      (2 * windowWidth) / 3,
      300
    );
  }
  /*if (frameCount < 2) {
    //https://p5js.org/reference/#/p5/text
    fill(0);
    textSize(20);
    text("w=" + img.width, 10, 300);
    text("h=" + img.height, 10, 320);
  }*/
}

// funcionan para mobile tmb (press & release)
function mousePressed() {
  //redraw(); // Run the code in draw one time (built-in)
  fingerMoving = true;
}
function mouseReleased() {
  //redraw(); // Run the code in draw one time (built-in)
  fingerMoving = false;
}
//https://github.com/IDMNYU/DM-GY-6063B-Creative-Coding/blob/master/mobile-examples/03_touch_coords/sketch.js
//https://p5js.org/reference/#/p5/touchMoved
// a veces en celus no detecta el mousePressed
function touchMoved() {
  fingerMoving = true;
  // block page scrolling
  return false;
}

function mouseWheel(ev) {
  // https://forum.processing.org/two/discussion/22345/how-do-i-zoom-in-using-p5
  // https://p5js.org/reference/#/p5/mouseWheel
  //zoom += sensativity * ev.delta;
  //zoom = constrain(zoom, zMin, zMax);
  changeZoom(ev.delta);
  // block page scrolling
  return false;
}
function setupPinch() {
  mc.on("pinchout pinchin", function(e) {
    pinching = true;
    //distance = int(dist(e.center.x, e.center.y, ex, ey));
    //console.log(e);
    //if (distance < diameter / 2) {
    if (e.type === "pinchin") changeZoom(+1);
    else if (e.type === "pinchout") changeZoom(-1);
    //}
  });
}

function changeZoom(val) {
  //diameter -= val;
  /*texSize -= val;
  //resizeImgs(imgs, texSize);
  for (var i = 0; i < mundos.length; i++) {
    var mundo = mundos[i];
    mundo.resizeRadius(texSize);
  }*/
  zoom -= val / 100;
}

// https://stackoverflow.com/questions/17242144/javascript-convert-hsb-hsv-color-to-rgb-accurately#17243070
/* accepts parameters
 * h  Object = {h:x, s:y, v:z}
 * OR
 * h, s, v
 */
// 0 <= h, s, v <= 1
//function HSVtoRGB(h, s, v) {
function HSLtoRGB(h, s, v) {
  var r, g, b, i, f, p, q, t;
  if (arguments.length === 1) {
    (s = h.s), (v = h.v), (h = h.h);
  }
  i = Math.floor(h * 6);
  f = h * 6 - i;
  p = v * (1 - s);
  q = v * (1 - f * s);
  t = v * (1 - (1 - f) * s);
  switch (i % 6) {
    case 0:
      (r = v), (g = t), (b = p);
      break;
    case 1:
      (r = q), (g = v), (b = p);
      break;
    case 2:
      (r = p), (g = v), (b = t);
      break;
    case 3:
      (r = p), (g = q), (b = v);
      break;
    case 4:
      (r = t), (g = p), (b = v);
      break;
    case 5:
      (r = v), (g = p), (b = q);
      break;
  }
  return {
    r: Math.round(r * 255),
    g: Math.round(g * 255),
    b: Math.round(b * 255)
  };
}
